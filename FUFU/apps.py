from django.apps import AppConfig


class FufuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'FUFU'
