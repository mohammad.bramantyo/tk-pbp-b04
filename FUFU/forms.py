from django import forms
from .models import Fufu

class FufuForm(forms.ModelForm):
    class Meta:
        model = Fufu
        fields = [
            "From",
            "Message"
        ]
