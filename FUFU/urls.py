from django.urls import path
from .views import getFufuById,addNewFufu,updateFufu,deleteFufu,fufuPage, fufuJson, getFufuById, myFufu, delFufu, editFufu, addFufu, getFufu, getMyFufu

urlpatterns = [
    path('fufu/', fufuPage, name="fufu"),
    path('fufuJson/', fufuJson, name="fufuJson"),
    path('getFufu/', getFufu, name="getFufu"),
    path('getMyFufu/', getMyFufu, name="getMyFufu"),
    path('getFufuById/', getFufuById, name="getFufuById"),
    path('addNewFufu/', addNewFufu, name="addNewFufu"),
    path('updateFufu/', updateFufu, name="updateFufu"),
    path('deleteFufu/', deleteFufu, name="deleteFufu"),
    path("myfufu/", myFufu, name="myfufu"),
    path("add/", addFufu, name="addFufu"),
    path("<int:id>/edit", editFufu, name="editFufu"),
    path("<int:id>/delete", delFufu, name="delFufu")
]