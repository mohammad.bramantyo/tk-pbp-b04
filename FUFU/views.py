from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http.response import HttpResponseRedirect
from django.http import HttpResponse
from django.core import serializers
from .models import Fufu
from django.contrib.auth.decorators import login_required
from .forms import FufuForm
from django.http import JsonResponse

def fufuPage(request):
    fufus = reversed(Fufu.objects.all().values())
    response = {}
    counter = 0
    lstFufu = []
    groupedFufu = []
    for fufu in fufus:
        if counter == 6:
            lstFufu.append(fufu)
            groupedFufu.append(lstFufu)
            lstFufu = []
            counter = 0
            continue
        lstFufu.append(fufu)
        counter += 1

    if len(lstFufu) != 0:
        groupedFufu.append(lstFufu)
        lstFufu = []

    response['groupedFufu'] = groupedFufu
    return render(request, 'fufu_index.html', response)

def fufuJson(request):
    fufus = reversed(Fufu.objects.all())
    data = serializers.serialize('json', fufus)
    return HttpResponse(data, content_type="application/json")

def getFufu(request):
    fufus = Fufu.objects.all().values()
    list_fufu = []
    for fufu in fufus:
        list_fufu.append(fufu)
    response = {"response":list_fufu,"meta":{'code':200,'message':"Request Success"}}
    return JsonResponse(response)

def getMyFufu(request):
    if request.method == 'POST':
        fufus = Fufu.objects.filter(Owner=request.POST.get('owner')).values()
        list_fufu = []
        for fufu in fufus:
            list_fufu.append(fufu)
        response = {"response":list_fufu,"meta":{'code':200,'message':"Request Success"}}
        return JsonResponse(response)
    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

def addNewFufu(request):
    if request.method == 'POST':
        form = FufuForm(request.POST or None)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.Owner = request.POST.get("Owner")
            obj.save()
            response = {"response": [],"meta":{'code':201,'message':"Fufu successfully created"}}
            return JsonResponse(response)
        else:
            response = {"response":[],"meta":{'code':203,'message':'Data Not Valid'}}
            return JsonResponse(response)
    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

def getFufuById(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        fufus = Fufu.objects.filter(id=id).values()
        data = []
        for fufu in fufus:
            data.append(fufu)
        response = {"response": fufu,"meta":{'code':200,'message':"Request Success"}}
        return JsonResponse(response)
    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

def updateFufu(request):
    if request.method == 'POST':
        id = request.POST.get("id")
        fufu = get_object_or_404(Fufu, id=id)
        fufu_form = FufuForm(request.POST or None, instance=fufu)
        if fufu_form.is_valid():
            fufu_form.save()
            response = {"response": [],"meta":{'code':200,'message':"Fufu successfully updated"}}
            return JsonResponse(response)
        else:
            response = {"response":[],"meta":{'code':203,'message':'Data Not Valid'}}
            return JsonResponse(response)
    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

def deleteFufu(request):
    if request.method == 'POST':
        id = request.POST.get("id")
        fufu = get_object_or_404(Fufu, id=id)
        fufu.delete()
        response = {"response": [],"meta":{'code':200,'message':"Fufu successfully deleted"}}
        return JsonResponse(response)
    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

@login_required(login_url="/login/")
def myFufu(request):
    fufus = Fufu.objects.filter(Owner=request.user.id)
    return render(request, "index_my_fufu.html", {"fufus": fufus})

@login_required(login_url="/login/")
def addFufu(request):
    context = {}

    form = FufuForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.Owner = request.user.id
        obj.save()
        return HttpResponseRedirect("/myfufu")

    context["form"] = form
    return render(request, "fufu_form.html", context)

@login_required(login_url="/login/")
def editFufu(request, id):
    context = {}
    fufu = get_object_or_404(Fufu, id=id)

    fufu_form = FufuForm(request.POST or None, instance=fufu)

    if fufu_form.is_valid():
        fufu_form.save()
        return HttpResponseRedirect("/myfufu")

    context["form"] = fufu_form

    return render(request, "fufu_form.html", context)

@login_required(login_url="/login/")
def delFufu(request, id):
    fufu = get_object_or_404(Fufu, id=id)
    fufu.delete()
    return HttpResponseRedirect("/myfufu")
