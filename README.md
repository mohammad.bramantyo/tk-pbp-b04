# Zona Hijau

- Anggota kelompok B-04 

1. Fadhli Gazalba - 2006525904
2. Kesya Aurelya - 2006486046
3. Mochamad Thariq Zahir Abdillah - 2006486185
4. Mohammad Bramantyo Putra Kusuma - 2006520903
5. Pramudya Wibisono - 2006526106
6. Salma Karimah - 2006529934
7. Syahdan Putra Adriatama - 2006486001

- Link herokuapp = https://zonahijau.herokuapp.com


- Cerita Aplikasi yang Diajukan serta Manfaatannya

Tak terasa, pandemi Covid-19 sudah hampir dua tahun “menemani” masyarakat Indonesia. Pandemi Covid-19 memberikan dampak yang sangat besar pada seluruh sektor esensial di dunia, khususnya Indonesia. Lambat laun, pemerintah Indonesia meminta agar masyarakat harus siap untuk hidup berdampingan dengan Covid-19. Peluncuran aplikasi PeduliLindungi merupakan salah satu aksi nyata pemerintah untuk menekan laju Covid-19 dan sebagai persiapan bagi masyarakat untuk hidup berdampingan dengan Covid-19. Oleh karena itu, kelompok kami membuat sebuah aplikasi yang dinamakan Zona Hijau. Sesuai namanya, website ini memiliki esensi agar Indonesia dapat terbebas dari wabah Covid-19 (zona hijau) di seluruh wilayah Indonesia. Zona Hijau diharapkan mampu membantu masyarakat dalam hidup berdampingan dengan Covid-19 sekaligus untuk mengurangi wabah tersebut.

Zona Hijau merupakan suatu aplikasi yang bertujuan untuk memberikan informasi dan edukasi mengenai Covid-19. Untuk mencapai tujuan tersebut, aplikasi ini akan menampilkan data-data penting terkait persebaran Covid-19, seperti data kasus hari ini, jumlah kenaikan kasus, daftar rumah sakit atau puskesmas rujukan, jawaban dari pertanyaan-pertanyaan seputar Covid-19 yang sering ditanyakan, dan lainnya. Selain memberikan informasi data, aplikasi ini juga memberikan konten-konten edukasi berbentuk infografis dan video mengenai tips kesehatan dan konten lain yang bertujuan untuk meningkatkan kesadaran dan pengetahuan masyarakat terhadap persebaran Covid-19. Selain itu, website ini juga menyediakan fitur-fitur yang melibatkan pengguna secara langsung melalui fitur pesan di mana pengguna dapat berbagi pengalaman, pengetahuan, ataupun perhatian kepada pengguna lainnya.


- Modul yang akan Diimplementasikan

1. Dashboard 

Pada halaman ini, pengguna akan mendapatkan informasi mengenai data kasus Covid-19 di Indonesia secara real time. Tips menghindari covid berupa infografis dan video akan ditampilkan juga pada dashboard.

2. Tips Menghindari Covid (infografis & video)

Pada fitur ini, pengguna dapat melihat konten edukasi mengenai Covid-19 berupa video dan infografis mengenai tips menghindari Covid-19. 

3. Frequently Asked Questions (FAQ) 

Pada fitur ini, permasalahan mengenai Covid-19 yang sering ditanyakan akan ditampilkan beserta dengan jawabannya sehingga pengguna tidak perlu repot untuk menanyakan hal-hal umum yang berkaitan dengan Covid-19. Pengguna dapat melakukan vote terhadap pertanyaan dan vote yang terbanyak akan  
ditampilkan paling atas.

4. Pesan dari Pengguna (FUFU: From Us For Us)

Pada fitur ini, pengguna dapat mengirimkan pesan seputar Covid-19 seperti tips, pengalaman, ataupun pesan yang nantinya akan ditampilkan pada slider di section FUFU berupa card. Pengguna harus melakukan login terlebih dahulu. 

5. My FUFU  

Pada fitur ini pengguna dapat mengedit maupun menghapus pesannya yang sudah di-post di section FUFU. FUFU pengguna akan ditampilkan berupa card yang disusun dengan grid. Untuk mengakses fitur ini pengguna harus melakukan login terlebih dahulu. 

6. Daftar Rumah Sakit atau Puskesmas Rujukan di Jabodetabek

Pada fitur ini, pengguna dapat mencari daftar rumah sakit rujukan Covid-19 di Jabodetabek yang disertai dengan alamat dan kontak dari rumah sakit rujukan tersebut. Selain itu, pengguna juga dapat melakukan pencarian daftar rumah sakit yang sesuai dengan wilayah yang diinginkan. 

7. Mitos atau Fakta

Pada fitur ini terdapat kuis yang membahas mengenai mitos dan fakta seputar Covid-19 yang beredar pada masyarakat dan di akhir kuis akan diberikan pembahasan. Setelah itu, skor pengguna akan dimasukkan ke leaderboard. Untuk mengakses fitur ini pengguna harus melakukan login terlebih dahulu.


- PERSONA

Target pengguna dari website ini adalah seluruh masyarakat Indonesia yang saat ini tengah menghadapi pandemi Covid-19. Dengan mewabahnya virus Covid-19, masyarakat Indonesia harus siap untuk hidup berdampingan dengan Covid-19. Pengguna dapat menggunakan website ini untuk mendapatkan informasi terbaru mengenai Covid-19, terutama data mengenai persebaran Covid-19 di Indonesia, jawaban dari pertanyaan-pertanyaan yang kerap ditanyakan melalui fitur Frequently Asked Questions (FAQ), dan daftar rumah sakit atau puskesmas rujukan. Selain itu, pengguna juga akan mendapatkan informasi mengenai tips-tips kesehatan dan pencegahan Covid-19 dalam bentuk infografis dan video. 

Terdapat dua tipe pengguna aplikasi berbeda yang dapat mengakses website kami. Tipe pengguna pertama adalah pengguna yang melakukan registrasi/login, sedangkan tipe kedua adalah pengguna yang belum melakukan registrasi. Pengguna yang telah melakukan registrasi dapat mengakses lebih banyak fitur daripada pengguna yang tidak melakukan registrasi, seperti fitur FUFU (From Us For Us) di mana pengguna dapat mengirimkan pesan seperti tips menghindari Covid-19, pengalaman selama pandemi, ataupun sekadar memberikan semangat kepada pengguna lainnya yang telah membuat akun. Pengguna tersebut juga bisa memanfaatkan fitur My FUFU yang akan memiliki akses untuk melihat, menghapus, dan mengedit pesan yang sudah ia buat sebelumnya. Berbeda dengan pengguna yang sudah memiliki akun, pengguna yang belum melakukan registrasi hanya dapat mengakses fitur-fitur biasa, yaitu informasi terkini data Covid-19 dan infografis lainnya secara umum. 

- Pembagian Kerja

1. Fadhli Gazalba [2006525904]: Mitos atau Fakta
2. Kesya Aurelya [2006486046]: MyFUFU dan add FUFU baru
3. Mochamad Thariq Zahir Abdillah [2006486185]: Infografis dan Video
4. Mohammad Bramantyo Putra Kusuma [2006520903]: List RS dan navbar
5. Pramudya Wibisono [2006526106]: Frequently Asked Question
6. Salma Karimah [2006529934]: Dashboard+feedback
7. Syahdan Putra Adriatama [2006486001]: login user, navbar, FUFU
