from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext, gettext_lazy as _
from django import forms

class CreateUserForm(UserCreationForm):
    error_messages = {
        'password_mismatch': "Password missmatch",
    }
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']
        error_messages = {
            'username': {
                'unique': 'Username is already taken',
            },
        }