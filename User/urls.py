from django.urls import path
from .views import registerPage, loginPage, logoutUser, loginFromFlutter, logoutFromFlutter, getUsernameFromFlutter, registerUser, loginUser

urlpatterns = [
	path('register/', registerPage, name="register"),
	path('registerUser/', registerUser, name="registerUser"),
	path('loginUser/', loginUser, name="loginUser"),
	path('login/', loginPage, name="login"),
	path('logout/', logoutUser, name="logout"),
	path('loginFromFlutter/', loginFromFlutter,name="loginFromFlutter"),
    path('logoutFromFlutter/', logoutFromFlutter, name="logoutFromFlutter"),
    path('getUsernameFromFlutter/', getUsernameFromFlutter, name="getUsernameFromFlutter")
]
