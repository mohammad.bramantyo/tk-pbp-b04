from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .forms import CreateUserForm
from .models import User
from django.http import JsonResponse

# Create your views here.

def getUsernameFromFlutter(request):
    print(request)
    print(request.user)
    return JsonResponse({
              "status": True,
              "message": "Successfully asked username",
              "username" : request.user.username,
              "score" : request.user.profile.score_temp
    }, status=200)

@csrf_exempt
def loginFromFlutter(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return JsonResponse({
              "status": True,
              "message": "Successfully Logged In!",
              "username" : username,
              "user_id" : user.id
            }, status=200)
        else:
            return JsonResponse({
              "status": False,
              "message": "Failed to Login, Account Disabled."
            }, status=401)

    else:
        return JsonResponse({
          "status": False,
          "message": "Failed to Login, check your email/password."
        }, status=401)

@csrf_exempt
def logoutFromFlutter(request):
    logout(request)
    return JsonResponse({
        "status": True,
        "message": "Successfully Logged out!",
        }, status=200)

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('../') # isi stringnya dengan url dashboard
    else:
        form = CreateUserForm()

        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Your account was successfully created")
                return redirect('login')
        response = {'form' : form, 'errors': form.errors.values()}
        return render(request,'user_register.html', response)

def registerUser(request):
    form = CreateUserForm()
    if request.method == 'POST':
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                # messages.success(request, "Your account was successfully created")
                username = request.POST.get('username')
                users = User.objects.filter(username=username).values()
                data = []
                for user in users:
                    data.append(user)
                response = {"response": data,"meta":{'code':201,'message':"Your account was successfully created"}}
                return JsonResponse(response)
            else:
                response = {"response":[],"meta":{'code':203,'message':'Username Already Registered Or Password Not Valid'}}
                return JsonResponse(response)

    else:
        response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
        return JsonResponse(response)

def loginUser(request):
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                users = User.objects.filter(username=username).values()
                data = []
                for user in users:
                    data.append(user)
                response = {"response":data,"meta":{'code':200,'message':"Login Success"}}
                return JsonResponse(response)
            else:
                response = {"response":[],"meta":{'code':203,'message':"Username or password is incorrect"}}
                return JsonResponse(response)
        else:
            response = {"response":[],"meta":{'code':301,'message':"Method Not Allowed"}}
            return JsonResponse(response)

def loginPage(request):
    nextUrl = request.GET.get('next', '')
    if request.user.is_authenticated:
        if nextUrl == '':
            return redirect('../') # isi stringnya dengan url dashboard
    else:
        if request.method == 'POST':
            nextUrl = request.POST.get('next', '')
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                if nextUrl == "":
                    return redirect('../') # isi stringnya dengan url dashboard
                return redirect(nextUrl) # isi string nya dengan next
            else:
                messages.info(request, 'Username or password is incorrect')
        response = {}
        return render(request, 'user_login.html', response)

def logoutUser(request):
    logout(request)
    return redirect('login')

