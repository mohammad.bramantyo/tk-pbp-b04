$(document).ready(function(){
    $("button").click(function(){
        $(".card-container").empty();
        var x = document.getElementById("daerah").value;
        $("#daerah").val("");
        
        $.ajax({
            url: "/rumahsakit/json",
            success: function(result){
                for(i=0;i<result.length;i++){
                    var rumahsakit = result[i].fields;
                    if(x=="Semua"){
                        $(".card-container").append(
                            '<div class="column"><div class="card"><div class="card-body">'+
                                '<h5 class="card-title"><b>'+rumahsakit.nama+'</b></h5>'+
                                '<p class="card-text"><img src="/static/rumahsakit/pin.png" height="19px" width="17.7px" style="margin-right: 10px;"> '+
                                rumahsakit.alamat+'</p>'+
                                '<p class="card-text"><img src="/static/rumahsakit/telephone.png" height="19px" width="17.7px" style="margin-right: 10px;">'+
                                rumahsakit.nomor_telepon+'</p>'+
                                '</div></div></div>');
                    }
                    else if(rumahsakit.daerah == x) {
                        $(".card-container").append(
                            '<div class="column"><div class="card"><div class="card-body">'+
                                '<h5 class="card-title"><strong>'+rumahsakit.nama+'</strong></h5>'+
                                '<p class="card-text"><img src="/static/rumahsakit/pin.png" height="19px" width="17.7px" style="margin-right: 10px;">'+rumahsakit.alamat+'</p>'+
                                '<p class="card-text"><img src="/static/rumahsakit/telephone.png" height="19px" width="17.7px" style="margin-right: 10px;">'+rumahsakit.nomor_telepon+'</p>'+
                                '</div></div></div>');
                            }
                        }
                    }
        });
    });
});

