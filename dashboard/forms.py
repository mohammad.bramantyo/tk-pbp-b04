from .models import Pesan
from django import forms

class FormPesan(forms.ModelForm):
    class Meta:
        model = Pesan
        fields = "__all__"
