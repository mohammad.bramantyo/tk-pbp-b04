from django.db import models
from django.db.models.fields import CharField

# Create your models here --> < pesan, saran, ratingApp >
class Pesan(models.Model):
    title = models.CharField(max_length=30)
    pesan = models.TextField()
