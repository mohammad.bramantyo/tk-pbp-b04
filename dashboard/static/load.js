$(document).ready(function(){
    console.log("test")
    $("#load").on('click',function(){
        var _curr=$(".box").length;
        var _limit=$(this).attr('limit');
        var _total=$(this).attr('total');
        console.log(_curr,_limit,_total)
        $.ajax({
            url:'/load-more',
            data:{
                limit:_limit,
                start:_curr
            },
            dataType:'json',
            beforeSend:function(){
                $("#load").attr('disabled',true);
            },
            success:function(res){
                $("#loadedData").append(res.data);
                $("#load").attr('disabled',false);

                var _totalData=$(".box").length;
                if(_totalData==_total){
                    $("#load").remove();
                }
            }
        });
    });
});