from django.urls import path
from .views import coba, load_more, view_saran

# Mengalihkan URL ke method dalam views yang bersesuaian 
urlpatterns = [
    path('', coba, name='dashboard'),
    path('load-more/', load_more, name='load-more'),
    path('view/', view_saran, name='view')
]
