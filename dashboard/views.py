from django.http import response
from django.http.response import HttpResponse 
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from requests.sessions import Request
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import Pesan
from .forms import FormPesan
import requests



# Create your views here.
def coba(request):
    response = requests.get('https://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/')
    data_prov = response.json()
    kasus =  sorted(data_prov, key=lambda d: d['provinsi']) 
    
    response2 = requests.get('https://api.kawalcorona.com/indonesia')
    data2 = response2.json()

    form = FormPesan(request.POST or None)
    if((form.is_valid) and (request.method == 'POST')):
        form.save()
        return HttpResponseRedirect('view')
    else:
        return render(request, 'coba2.html', {'data': kasus[:4], 'data2': data2, 'form':form})

def load_more(request):
    offset=int(request.GET['start'])
    limit=int(request.GET['limit'])
    
    response = requests.get('https://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/')
    data_prov = response.json()
    kasus =  sorted(data_prov, key=lambda d: d['provinsi']) 
    data=kasus[offset:offset+limit]

    hasil=render_to_string('ajax/coba2.html',{'data':data})
    return JsonResponse({'data':hasil})

@login_required(login_url="/login/")
def view_saran(request):
    pesan = Pesan.objects.all().values()
    response = {'pesan':pesan}
    return render(request, 'kritik_saran.html', response)
    
