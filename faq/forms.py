from django import forms
from .models import FAQ

class FAQForm(forms.ModelForm):
    class Meta:
        model = FAQ
        fields = ['question', 'answer']
    
    question_attrs = {
        'type':'text',
        'placeholder':'Write your question here..',
        'class':'form-control'
    }

    answer_attrs = {
        'type':'text',
        'placeholder':'Write your answer here..',
        'class':'form-control'
    }

    question = forms.CharField(label="question",required=True,
    max_length=100,widget=forms.TextInput(attrs=question_attrs))

    answer = forms.CharField(label="answer",required=True,
    widget=forms.Textarea(attrs=answer_attrs))