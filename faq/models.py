from django.db import models

# Create your models here.
class FAQ(models.Model):
    question = models.CharField(max_length=100)
    answer = models.TextField()
    likeNum = models.IntegerField(default=0)