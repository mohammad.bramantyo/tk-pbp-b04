from django.urls import path
from .views import faq_index, faq_json, faq_add, faq_update_like, add_from_flutter

app_name = 'faq'
urlpatterns = [
    path('faq/', faq_index, name='faq_index'),
    path('faq/json/', faq_json, name='faq_json'),
    path('faq/add', faq_add, name='faq_add'),
    path('faq/update_like/<int:id>', faq_update_like),
    path('faq/add_from_flutter', add_from_flutter)
]