from django.shortcuts import render
from .models import FAQ
from .forms import FAQForm
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import QueryDict
from django.core import serializers
from django.contrib.auth.decorators import login_required
import json

# Create your views here.
def faq_index(request):
    faqs = FAQ.objects.order_by('-likeNum')
    response = {'faqs': faqs}
    return render(request, 'faq.html', response)

def faq_json(request):
    # sorting
    data = serializers.serialize('json', FAQ.objects.order_by('-likeNum'))

    return HttpResponse(data, content_type="application/json")

def faq_update_like(request, id):
    faq = FAQ.objects.get(pk=id) # get object

    # increment like
    faq.likeNum += 1
    faq.save(update_fields=['likeNum'])

    # sorting
    data = serializers.serialize('json', FAQ.objects.order_by('-likeNum'))

    return HttpResponse(data, content_type="application/json")

@login_required(login_url='/login/')
def faq_add(request):
    if request.method == 'POST':
        form = FAQForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/faq')
    else:
        form = FAQForm()

    return render(request, 'form_faq.html', {'form': form})

def add_from_flutter(request):

    if(request.method=='POST'):
        request_data = request.body
        request_data = json.loads(request_data.decode('utf-8'))

        qdict_data = QueryDict('', mutable=True)
        qdict_data.update(request_data)

        flutter_form = FAQForm(qdict_data)
        flutter_form.save()

        return HttpResponseRedirect('/faq/')
