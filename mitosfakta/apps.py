from django.apps import AppConfig


class MitosfaktaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mitosfakta'
