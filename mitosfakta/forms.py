from django import forms
from django.forms import fields, widgets
from .models import Question

def placeholder():
    lastobj = Question.objects.latest('nomor')
    return lastobj.nomor

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = '__all__'

    question_form = {
            'type':'text',
            'placeholder':'Masukkan pertanyaan',
            'class':'form-control'
        }
    answer_form = {
        'class':'form-control'
    }
    mitos_fakta = (
        ("mitos","mitos"),
        ("fakta","fakta")
    )
    pembahasan_form = {
        'type':'text',
        'placeholder':'Masukkan pembahasan',
        'class':'form-control'
    }
    nomor_forms = {
        'type':'number',
        'placeholder':'Masukkan nomor soal',
        'class':'form-control'
    }

    question = forms.CharField(label="question",required=True,max_length=1000,widget=forms.TextInput(attrs=question_form))
    answer = forms.ChoiceField(choices=mitos_fakta,required=True)
    pembahasan = forms.CharField(label="pembahasan",required=True,max_length=1000,widget=forms.TextInput(attrs=pembahasan_form))
    nomor = forms.CharField(label="nomor",required=True,max_length=1000,widget=forms.TextInput(attrs=nomor_forms))
