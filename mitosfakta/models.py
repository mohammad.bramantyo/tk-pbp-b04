from django.db import models

# Create your models here.
class Question(models.Model):
    question = models.CharField(max_length=1000)
    answer = models.CharField(max_length=10)
    pembahasan = models.CharField(max_length=1000)
    nomor = models.CharField(max_length=1000, default=0)
