window.onload = initall;
var saveAnsButton;

function initall() {
    saveAnsButton = document.getElementById('save_ans');
    saveAnsButton.onclick = saveans;
}

function saveans() {
    document.getElementById('save_ans').disabled = true;
    document.getElementById('submitAnswer').disabled = false;
    var ans = document.querySelector('input[name=name]:checked').value;
    var req = new XMLHttpRequest();
    var url = 'saveans?ans='+ans;
    req.open("GET", url, true);
    req.send();
}
