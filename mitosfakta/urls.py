from django.urls import include, path
from .views import index, quiz, result, leaderboard, saveans, add_question, json_soal, json_leaderboard, add_flutter, update_score

urlpatterns = [
    path('mitosfakta/', index, name='index'),
    path('mitosfakta/quiz', quiz, name='quiz'),
    path('mitosfakta/result', result, name='result'),
    path('mitosfakta/leaderboard', leaderboard, name='leaderboard'),
    path('mitosfakta/saveans', saveans, name='saveans'),
    path('mitosfakta/add', add_question, name='add_question'),
    path('mitosfakta/json_soal', json_soal, name='json_soal'),
    path('mitosfakta/json_leaderboard', json_leaderboard, name='json_leaderboard'),
    path('mitosfakta/add_flutter', add_flutter, name='add_flutter'),
    path('mitosfakta/update_score', update_score, name='update_score')
]
