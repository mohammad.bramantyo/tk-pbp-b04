from django.http import response, QueryDict, JsonResponse
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from .models import Question
from django.core.paginator import EmptyPage, InvalidPage, Paginator
from django.contrib.auth.decorators import login_required
from User.models import Profile
from mitosfakta.forms import QuestionForm
from django.core import serializers
import json

@login_required(login_url="/login/")
def index(request):
    temp = 0
    Profile.objects.filter(user=request.user).update(num_ans=temp)
    Profile.objects.filter(user=request.user).update(score_temp=temp)
    return render(request, 'mitosfakta_index.html')

@login_required(login_url="/login/")
def quiz(request):
    obj = Question.objects.all()
    last = Question.objects.last()
    paginator = Paginator(obj, 1)
    try:
        page = int(request.GET.get('page', '1'))
    except:
        page = 1
    try:
        questions = paginator.page(page)
    except(EmptyPage, InvalidPage):
        questions = paginator.page(paginator.num_pages)
    return render(request,'mitosfakta_quiz.html', {'obj':obj, 'questions':questions, 'last':last})

def saveans(request):
    anslist = []
    obj = Question.objects.all()
    for i in obj:
        anslist.append(i.answer)
    index = request.user.profile.num_ans
    ans = request.GET['ans']
    if ans == anslist[index]:
        score = request.user.profile.score_temp
        score += 1
        Profile.objects.filter(user=request.user).update(score_temp=score)
    index += 1
    Profile.objects.filter(user=request.user).update(num_ans=index)
    return HttpResponse()

def result(request):
    anslist = []
    obj = Question.objects.all()
    for i in obj:
        anslist.append(i.answer)
    qcount = len(anslist)
    score = request.user.profile.score_temp
    score = score * (100 / qcount)
    score = int(score)
    Profile.objects.filter(user=request.user).update(score=score)
    return render(request, 'mitosfakta_result.html', {'obj':obj, 'score':score})

def update_score(request):
    request_data = request.body
    request_data = json.loads(request_data.decode('utf-8'))
    username = request_data['username']
    score = request_data['score']
    score = int(score)
    obj = Profile.objects.all()
    for i in obj:
        if i.user.username == username:
            Profile.objects.filter(user=i.user).update(score=score)
    return JsonResponse({
              "status": True,
              "message": "Successfully updated score",
    }, status=200)

def leaderboard(request):
    profile = Profile.objects.all().order_by('-score')
    return render(request, 'mitosfakta_leaderboard.html', {'data':profile})

def json_leaderboard(request):
    leaderboard_score = Profile.objects.all().order_by('-score')
    rank = 1
    lst = []
    for i in leaderboard_score:
        if i.score == 0:
            break
        dic = {}
        dic["rank"] = rank
        dic["name"] = i.user.username
        dic["score"] = i.score
        lst.append(dic)
        rank += 1
    json_str = json.dumps(lst)
    return HttpResponse(json_str, content_type="application/json")

def json_soal(request):
    soal = Question.objects.all()
    data = serializers.serialize('json', soal)
    return HttpResponse(data, content_type="application/json")

def add_flutter(request):
    if(request.method=='POST'):
        request_data = request.body
        request_data = json.loads(request_data.decode('utf-8'))

        qdict_data = QueryDict('',mutable=True)
        qdict_data.update(request_data)

        flutter_form = QuestionForm(qdict_data)

        if flutter_form.is_valid():
            flutter_form.save()

        return HttpResponseRedirect('/mitosfakta/')


@login_required(login_url="/login/")
def add_question(request):
    context = {}
    form = QuestionForm(request.POST or None)

    if form.is_valid() and request.method=='POST':
        form.save()
        return HttpResponseRedirect('/mitosfakta/')
    else:
        context['form'] = form
        return render(request,'mitosfakta_form.html',context)
