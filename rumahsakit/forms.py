from django import forms
from django.forms import fields, widgets
from .models import rumahsakit

class rsForm(forms.ModelForm):
    class Meta:
        model = rumahsakit
        fields = '__all__'

    nama_attrs = {
            'type':'text',
            'placeholder':'Nama Rumah Sakit',
            'class':'form-control'
        }
    alamat_attrs = {
        'type':'text',
        'placeholder':'Alamat',
        'class':'form-control'
    }
    tlp_attrs = {
        'type':'text',
        'placeholder':'Nomor telepon',
        'class':'form-control'
    }
    daerah_attrs = {
        'class':'form-control'
    }
    daerah_choices = (
        ("Jakarta","Jakarta"),
        ("Bogor","Bogor"),
        ("Depok","Depok"),
        ("Tangerang","Tangerang"),
        ("Bekasi","Bekasi"),
    )

    nama = forms.CharField(label="nama",required=True,
    max_length=50,widget=forms.TextInput(attrs=nama_attrs))

    daerah = forms.ChoiceField(choices=daerah_choices,required=True)

    alamat = forms.CharField(label="Alamat",required=True,
    max_length=200,widget=forms.TextInput(attrs=alamat_attrs))

    nomor_telepon = forms.CharField(label="No. Telepon",required=True,
    max_length=16,widget=forms.TextInput(attrs=tlp_attrs))



