from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.

class rumahsakit(models.Model):
    daerah_choices = (
        ("Jakarta","Jakarta"),
        ("Bogor","Bogor"),
        ("Depok","Depok"),
        ("Tangerang","Tangerang"),
        ("Bekasi","Bekasi"),
    )
    nama = models.CharField(max_length=100)
    daerah = models.CharField(choices=daerah_choices,max_length=20,default="Jakarta")
    alamat = models.CharField(max_length=200)
    nomor_telepon = models.CharField(max_length=16)


