from django.urls import include, path
from .views import index, json_rs, add_rs,add_from_flutter

urlpatterns = [
    path('rumahsakit/', index, name='index'),
    path('rumahsakit/json/', json_rs , name='json'),
    path('rumahsakit/add/', add_rs, name='add_rs'),
    path('rumahsakit/add_from_flutter', add_from_flutter, name='add_from flutter'),    
]