from django.http import response, QueryDict
from django.shortcuts import render
from django.core import serializers
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from rumahsakit.forms import rsForm
from .models import rumahsakit
import json


# Create your views here.
def index(request):
    daftarrumahsakit = rumahsakit.objects.all().values()
    response = {'daftarrumahsakit':daftarrumahsakit}
    return render(request,'index.html',response)

def json_rs(request):
    filter_data = request.GET['filter']

    if filter_data=='all':
        data=serializers.serialize('json',rumahsakit.objects.all())
    elif filter_data == 'Jakarta':
        data=serializers.serialize('json',rumahsakit.objects.all().filter(daerah='Jakarta'))
    elif filter_data == 'Bogor':
        data=serializers.serialize('json',rumahsakit.objects.all().filter(daerah='Bogor'))
    elif filter_data == 'Depok':
        data=serializers.serialize('json',rumahsakit.objects.all().filter(daerah='Depok'))
    elif filter_data == 'Tangerang':
        data=serializers.serialize('json',rumahsakit.objects.all().filter(daerah='Tangerang'))
    else:
        data=serializers.serialize('json',rumahsakit.objects.all().filter(daerah='Bekasi'))        

    return  HttpResponse(data, content_type='application/json')

def add_rs(request):
    context = {}
    form = rsForm(request.POST or None)

    if form.is_valid() and request.method=='POST':
        form.save()
        return HttpResponseRedirect('/rumahsakit/')

    else:
        context['form'] = form
        return render(request,'form_rs.html',context)

def add_from_flutter(request):
    
    if(request.method=='POST'):
        request_data = request.body
        request_data = json.loads(request_data.decode('utf-8'))

        qdict_data = QueryDict('',mutable=True)
        qdict_data.update(request_data)

        flutter_form = rsForm(qdict_data)
        flutter_form.save()

        return HttpResponseRedirect('/rumahsakit/')
