from django.apps import AppConfig


class VideoinfogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'videoinfog'
