from .models import Videos
from django import forms
from django.forms import fields, widgets
class VideoForm(forms.ModelForm):
    class Meta:
        model = Videos
        fields = ['tittle','url']

    tittle_attrs = {
        'type':'text',
        'placeholder':'Judul Video',
        'class':'form-control'
    }
    url_attrs = {
        'type':'url',
        'placeholder':'Link Video',
        'class':'form-control'
    }

    tittle = forms.CharField(label="tittle",required=True,
    max_length=100,widget=forms.TextInput(attrs=tittle_attrs))

    url = forms.CharField(label="url",required=True,
    max_length=300,widget=forms.URLInput(attrs=url_attrs))

