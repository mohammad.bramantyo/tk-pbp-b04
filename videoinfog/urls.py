from django.urls import path
from django.urls.resolvers import URLPattern
from .views import add_video_flutter, index, json, add_video

urlpatterns = [
    path('videoinfog/', index, name = 'index'),
    path('videoinfog/json/', json, name = 'json'),
    path('videoinfog/add/', add_video, name = 'add_video'),
    path('videoinfog/add_from_flutter/', add_video_flutter, name = 'add_from_flutter')
]