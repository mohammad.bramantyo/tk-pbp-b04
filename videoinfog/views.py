from django.http import response
from django.shortcuts import render
from .models import Videos
from django.core import serializers
from django.http.response import HttpResponse
from django.http import HttpResponseRedirect
from .forms import VideoForm
from django.http import response, QueryDict

# Create your views here.
def index(request):
    video = Videos.objects.all()
    response = {'videos' : video}
    return render(request, 'videoinfog_index.html', response)

def json(request):
    video = Videos.objects.all()
    data = serializers.serialize('json', video)
    return HttpResponse(data, content_type="application/json")

def add_video(request):
    context = {}
    form = VideoForm(request.POST or None)

    if form.is_valid() and request.method=='POST':
        form.save()
        return HttpResponseRedirect('/videoinfog/')

    else:
        context['form'] = form
        return render(request,'video_form.html',context)

def add_video_flutter(request):
    if(request.method=='POST'):
        request_data = request.body
        request_data = json.loads(request_data.decode('utf-8'))
        data = QueryDict('',mutable=True)
        data.update(request_data)
        form_req = VideoForm(data)

        if form_req.is_valid():
            form_req.save()

        return HttpResponseRedirect('/videoinfog/')